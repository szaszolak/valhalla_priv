SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: crop_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.crop_type AS ENUM (
    'grain',
    'cheese',
    'meat',
    'wool'
);


--
-- Name: month_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.month_enum AS ENUM (
    'january',
    'february',
    'march',
    'april',
    'may',
    'june',
    'july',
    'august',
    'september',
    'october',
    'november',
    'december'
);


--
-- Name: warrior_role; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.warrior_role AS ENUM (
    'shieldman',
    'spearman',
    'berserker'
);


--
-- Name: weapon_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.weapon_type AS ENUM (
    'axe',
    'spear',
    'sword'
);


--
-- Name: weather_rain; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.weather_rain AS ENUM (
    'heavy',
    'moderate',
    'none'
);


--
-- Name: weather_temperature; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.weather_temperature AS ENUM (
    'cold',
    'moderate',
    'hot'
);


--
-- Name: weather_wind; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.weather_wind AS ENUM (
    'strong',
    'moderate',
    'light'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: farmers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.farmers (
    id bigint NOT NULL,
    crop public.crop_type NOT NULL,
    harvest_time public.month_enum NOT NULL
);


--
-- Name: farmers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.farmers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: farmers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.farmers_id_seq OWNED BY public.farmers.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: villagers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.villagers (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    villagerable_id integer,
    villagerable_type character varying
);


--
-- Name: villagers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.villagers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: villagers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.villagers_id_seq OWNED BY public.villagers.id;


--
-- Name: warriors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.warriors (
    id bigint NOT NULL,
    raids integer DEFAULT 0,
    role public.warrior_role DEFAULT 'shieldman'::public.warrior_role,
    weapon_of_choice public.weapon_type DEFAULT 'axe'::public.weapon_type
);


--
-- Name: warriors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.warriors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: warriors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.warriors_id_seq OWNED BY public.warriors.id;


--
-- Name: weather_prophecies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.weather_prophecies (
    id bigint NOT NULL,
    created_at date,
    temperature public.weather_temperature DEFAULT 'moderate'::public.weather_temperature,
    wind public.weather_wind DEFAULT 'moderate'::public.weather_wind,
    rain public.weather_rain DEFAULT 'moderate'::public.weather_rain
);


--
-- Name: weather_prophecies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.weather_prophecies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: weather_prophecies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.weather_prophecies_id_seq OWNED BY public.weather_prophecies.id;


--
-- Name: farmers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.farmers ALTER COLUMN id SET DEFAULT nextval('public.farmers_id_seq'::regclass);


--
-- Name: villagers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.villagers ALTER COLUMN id SET DEFAULT nextval('public.villagers_id_seq'::regclass);


--
-- Name: warriors id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.warriors ALTER COLUMN id SET DEFAULT nextval('public.warriors_id_seq'::regclass);


--
-- Name: weather_prophecies id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.weather_prophecies ALTER COLUMN id SET DEFAULT nextval('public.weather_prophecies_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: farmers farmers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.farmers
    ADD CONSTRAINT farmers_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: villagers villagers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.villagers
    ADD CONSTRAINT villagers_pkey PRIMARY KEY (id);


--
-- Name: warriors warriors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.warriors
    ADD CONSTRAINT warriors_pkey PRIMARY KEY (id);


--
-- Name: weather_prophecies weather_prophecies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.weather_prophecies
    ADD CONSTRAINT weather_prophecies_pkey PRIMARY KEY (id);


--
-- Name: index_villagers_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_villagers_on_email ON public.villagers USING btree (email);


--
-- Name: index_villagers_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_villagers_on_reset_password_token ON public.villagers USING btree (reset_password_token);


--
-- Name: index_villagers_on_villagerable_type_and_villagerable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_villagers_on_villagerable_type_and_villagerable_id ON public.villagers USING btree (villagerable_type, villagerable_id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20190401175013'),
('20190401183349'),
('20190401184736'),
('20190408171946'),
('20190416171135');


