class CreateWarriors < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do 
        execute <<~SQL
          CREATE TYPE warrior_role AS ENUM ('shieldman', 'spearman', 'berserker');
          CREATE TYPE weapon_type AS ENUM ('axe', 'spear', 'sword');
        SQL
      end

      dir.down do
        execute <<~SQL
          DROP TYPE warrior_role;
          DROP TYPE weapon_type;
        SQL
      end
    end

    create_table :warriors do |t|
      t.integer :raids, default: 0
    end

    add_column :warriors, :role, 'warrior_role', default: 'shieldman'
    add_column :warriors, :weapon_of_choice, 'weapon_type', default: 'axe'
  end
end
