class AddPolymorphicAssociationToVillager < ActiveRecord::Migration[6.0]
  def change
    add_column :villagers, :villagerable_id, :integer
    add_column :villagers, :villagerable_type, :string

    add_index :villagers, [:villagerable_type, :villagerable_id]
  end
end
