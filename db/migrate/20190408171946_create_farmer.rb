class CreateFarmer < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        execute <<~SQL
          CREATE TYPE crop_type AS ENUM('grain', 'cheese', 'meat', 'wool');
          CREATE TYPE month_enum AS ENUM ('january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december');                              
        SQL
      end

      dir.down do
        execute <<~SQL
          DROP TYPE crop_type;
          DROP TYPE month_enum;
        SQL
      end
    end

    create_table :farmers

    add_column :farmers, :crop, 'crop_type', null: false
    add_column :farmers, :harvest_time, 'month_enum', null: false
  end
end
