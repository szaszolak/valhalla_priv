# frozen_string_literal: true

class CreateSeerWeatherProphecies < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        execute <<~SQL
          CREATE TYPE weather_temperature AS ENUM ('cold', 'moderate', 'hot');
          CREATE TYPE weather_wind AS ENUM ('strong', 'moderate', 'light');
          CREATE TYPE weather_rain AS ENUM ('heavy', 'moderate', 'none');
        SQL
      end

      dir.down do
        execute <<~SQL
          DROP TYPE weather_temperature;
          DROP TYPE weather_wind;
          DROP TYPE weather_rain;
        SQL
      end
    end

    create_table :weather_prophecies do |t|
      t.column :created_at, :date
    end

    add_column :weather_prophecies, :temperature, 'weather_temperature', default: 'moderate'
    add_column :weather_prophecies, :wind, 'weather_wind', default: 'moderate'
    add_column :weather_prophecies, :rain, 'weather_rain', default: 'moderate'
  end
end
