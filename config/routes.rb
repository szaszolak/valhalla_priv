Rails.application.routes.draw do
  resources :warriors, only: [:index, :new, :create]
  resources :farmers, only: [:index, :new, :create]
  devise_for :villagers
  root 'warriors#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  resource :weather_prophecies, only: [:show]
end
