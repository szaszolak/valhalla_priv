# frozen_string_literal: true

class WeatherProphecyService
  def call
    WeatherProphecy.find_by(created_at: Date.current) || generate
  end

  private

  attr_reader :datestamp

  def generate
    WeatherProphecy
      .create(
        rain:        forsee_rain,
        wind:        forsee_wind,
        temperature: forsee_temperature,
        created_at:  Date.current
      )
  end

  def forsee_rain
    rain_possible_values = WeatherProphecy.rains.values
    possible_values_cnt = rain_possible_values.size 
    rain_possible_values[rand(10_000) % possible_values_cnt]  
  end

  def forsee_wind
    winds_possible_values = WeatherProphecy.winds.values
    possible_values_cnt = winds_possible_values.size 
    winds_possible_values[rand(5_000) % possible_values_cnt]  
  end

  def forsee_temperature
    temperatures_possible_values = WeatherProphecy.temperatures.values
    possible_values_cnt = temperatures_possible_values.size 
    temperatures_possible_values[rand(2_500) % possible_values_cnt]  
  end
end
