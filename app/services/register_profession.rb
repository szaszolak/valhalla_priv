# frozen_string_literal: true

class RegisterProfession
  class << self
    def call(profession_class:, params:, villager:)
      new(profession_class, params, villager).call
    end
  end

  def initialize(profession_class, params, villager)
    @profession_class = profession_class
    @params = params
    @villager = villager
  end

  def call
    profession_class.transaction do
      remove_old_assigment
      assign_new_proffestion
    end
  end

  private

  attr_reader :profession_class, :params, :villager

  def remove_old_assigment
    (villager.villagerable || profession_class.new).delete
  end

  def assign_new_proffestion
    villager.villagerable = profession_class.new(params)
    villager.save!
  end
end
