# frozen_string_literal: true

class WarriorsController < ApplicationController
  def index
    render locals: { warriors: Warrior.all }
  end

  def new
    render locals: { warrior: Warrior.new }
  end

  def create
    RegisterProfession.call(profession_class: Warrior, params: warrior_params, villager: current_villager)
    redirect_to warriors_path, notice: 'Warrior was successfully created.'
  end

  private

  def warrior_params
    params.require(:warrior).permit(:role, :weapon_of_choice)
  end
end
