# frozen_string_literal: true

class WeatherPropheciesController < ApplicationController
  def show
    render locals: { prophecy: WeatherProphecyService.new.call }
  end
end
