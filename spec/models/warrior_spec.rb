# frozen_string_literal: true

# == Schema Information
#
# Table name: warriors
#
#  id               :bigint(8)        not null, primary key
#  raids            :integer          default(0)
#  role             :enum             default("shieldman")
#  weapon_of_choice :enum             default("axe")
#

require 'rails_helper'

RSpec.describe Warrior, type: :model do
  describe 'validation' do
    describe 'role:' do
      described_class.roles.values.each do |role|
        context role do
          it 'is truthy' do
            warrior = build(:warrior, role: role)
            expect(warrior.valid?).to be_truthy
          end
        end
      end

      context 'unknown type' do
        it 'raises argument error' do
          expect do 
            described_class.new(role: 'unknown') 
          end.to raise_error(ArgumentError, "'unknown' is not a valid role")
        end
      end

      context 'absent' do
        it 'is invalid' do
          expect(build(:warrior, role: nil).valid?).to be_falsey
        end
      end
    end

    describe 'weapon_of_choice:' do
      described_class.weapon_of_choices.values.each do |weapon|
        context weapon do
          it 'is truthy' do
            warrior = build(:warrior, weapon_of_choice: weapon)
            expect(warrior.valid?).to be_truthy
          end
        end
      end

      context 'unknown type' do
        it 'raises argument error' do
          expect do
             described_class.new(weapon_of_choice: 'unknown') 
          end.to raise_error(ArgumentError, "'unknown' is not a valid weapon_of_choice")
        end
      end

      context 'absent' do
        it 'is invalid' do
          expect(build(:warrior, weapon_of_choice: nil).valid?).to be_falsey
        end
      end
    end
  end
end
