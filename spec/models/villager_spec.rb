# frozen_string_literal: true

# == Schema Information
#
# Table name: villagers
#
#  id                     :bigint(8)        not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  villagerable_type      :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  villagerable_id        :integer
#
# Indexes
#
#  index_villagers_on_email                                  (email) UNIQUE
#  index_villagers_on_reset_password_token                   (reset_password_token) UNIQUE
#  index_villagers_on_villagerable_type_and_villagerable_id  (villagerable_type,villagerable_id)
#

require 'rails_helper'

RSpec.describe Villager, type: :model do
  describe 'validation' do
    context 'associations' do
      it 'fails upon invalid villagerable' do
        villager = build(:villager)
        villager.villagerable = build(:warrior, weapon_of_choice: nil)
        expect(villager.valid?).to be_falsey
      end

      it 'succed upon valid villagerable' do
        villager = build(:villager)
        villager.villagerable = build(:warrior)
        expect(villager.valid?).to be_truthy
      end
    end
  end
end
