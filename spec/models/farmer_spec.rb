# frozen_string_literal: true

# == Schema Information
#
# Table name: farmers
#
#  id           :bigint(8)        not null, primary key
#  crop         :enum             not null
#  harvest_time :enum             not null
#

require 'rails_helper'

RSpec.describe Farmer, type: :model do
  describe 'validation' do
    context 'crop' do
      described_class.crops.values.each do |crop|
        context crop do
          it 'is valid' do
            expect(build(:farmer, crop: crop).valid?).to be_truthy
          end
        end
      end

      context 'unknown type' do
        it 'raises argument error' do
          expect { described_class.new(crop: 'unknown') }.to raise_error(ArgumentError, "'unknown' is not a valid crop")
        end
      end

      context 'absent' do
        it 'is invalid' do
          expect(build(:farmer, crop: nil).valid?).to be_falsey
        end
      end
    end

    context 'harvest time' do
      described_class.harvest_times.values.each do |month|
        context month do
          it 'is valid' do
            expect(build(:farmer, harvest_time: month).valid?).to be_truthy
          end
        end
      end

      context 'unknown type' do
        it 'raises argument error' do
          expect { described_class.new(harvest_time: 'unknown') }.to raise_error(ArgumentError, "'unknown' is not a valid harvest_time")
        end
      end

      context 'absent' do
        it 'is invalid' do
          expect(build(:farmer, harvest_time: nil).valid?).to be_falsey
        end
      end
    end
  end
end
