# frozen_string_literal: true

require 'rails_helper'

RSpec.describe WeatherProphecyService do
  describe '#call' do
    context 'there is no prophecy for given date' do
      it 'creates new weather prophecy record' do
        expect { described_class.new.call }.to change { WeatherProphecy.count }.from(0).to(1)
      end
    end

    context 'there is prophecy for given date' do
      it 'does not create new weather prophecy record' do
        create(:weather_prophecy)
        expect { described_class.new.call }.to_not change { WeatherProphecy.count }
      end

      it 'returns exisiting prophecy record' do
        prophecy = create(:weather_prophecy)
        expect(described_class.new.call).to eql prophecy
      end
    end
  end
end
