# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RegisterProfession do
  describe 'self.call' do
    [Warrior, Farmer].each do |profession_class|
      context "profession class: #{profession_class}" do
        let(:correct_params) { attributes_for(profession_class.to_s.downcase) }
        let(:malformed_params) { correct_params.transform_values { 'i am wrong' } }

        context 'correct prams' do
          it "creates new #{profession_class.table_name} record" do
            villager = create(:villager)
            expect do
              described_class.call(profession_class: profession_class, params: correct_params, villager: villager)
            end.to change { profession_class.count }.from(0).to(1)
          end

          it 'removes stale profession' do
            villager = create(:villager, villagerable: build(profession_class.to_s.downcase))
            expect do
              described_class.call(profession_class: profession_class, params: correct_params, villager: villager)
            end.to_not change { profession_class.count }
          end

          it 'assings new profession to villager' do
            villager = create(:villager)
            expect do
              described_class.call(profession_class: profession_class, params: correct_params, villager: villager)
            end.to change { villager.villagerable }.from(nil).to(instance_of(profession_class))
          end
        end

        context 'malformed prams' do
          it 'raises error' do
            villager = create(:villager, villagerable: build(:warrior))
            expect do
              described_class.call(profession_class: profession_class, params: malformed_params, villager: villager)
            end.to raise_error ArgumentError
          end
        end
      end
    end
  end
end