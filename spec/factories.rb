# frozen_string_literal: true

FactoryBot.define do
  factory :villager do
    email    { Faker::Internet.unique.email }
    password { Faker::Crypto.md5 }
  end

  factory :warrior do
    villager
    role { Warrior.roles.values.first }
    weapon_of_choice { Warrior.weapon_of_choices.values.first }
  end

  factory :farmer do
    villager
    crop { Farmer.crops.values.first }
    harvest_time { Farmer.harvest_times.values.first }
  end
  
  factory :weather_prophecy do
    wind { WeatherProphecy.winds.values.first }
    temperature { WeatherProphecy.temperatures.values.first }
    rain { WeatherProphecy.rains.values.first }
    created_at { Date.current }
  end
end
