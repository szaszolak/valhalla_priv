# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Farmers', type: :request do
  let(:villager) { create(:villager) }

  before { sign_in villager }

  describe 'GET /farmers' do
    it 'list farmers' do
      farmer = create(:farmer)
      get farmers_path

      expect(response.body).to include(farmer.villager.email)
    end
  end

  describe 'GET /farmers/new' do
    it 'list farmers' do
      get new_farmer_path

      expect(response.body).to include('Crop')
      expect(response.body).to include('Harvest time')
    end
  end

  describe 'POST /farmers' do
    it 'creates new farmer' do
      farmer_params = attributes_for(:farmer)

      expect { post farmers_path, params: { farmer: farmer_params } }.to change { Farmer.count }.by(1)
      expect(response).to redirect_to(farmers_path)
    end
  end
end
