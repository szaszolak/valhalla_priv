# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Warriors', type: :request do
  let(:villager) { create(:villager) }

  before { sign_in villager }

  describe 'GET /warriors' do
    it 'list warriors' do
      warrior = create(:warrior)
      get warriors_path

      expect(response.body).to include(warrior.villager.email)
    end
  end

  describe 'GET /warriors/new' do
    it 'list farmers' do
      get new_warrior_path

      expect(response.body).to include('Weapon')
      expect(response.body).to include('Role')
    end
  end

  describe 'POST /warriors' do
    it 'creates new warrior' do
      warrior_params = attributes_for(:warrior)

      expect { post warriors_path, params: { warrior: warrior_params } }.to change { Warrior.count }.by(1)
      expect(response).to redirect_to(warriors_path)
    end
  end
end
